# test-09-06-2021



### Demo

```
https://www.loom.com/share/3cc0b3f440264995a5167b1596f4344d
```


## Project setup

- Go to Server Folder
- Follow Read Me

- Go to Client Folder
- Follow Read Me

## Set Up MongoDB

```
https://cloud.mongodb.com/
```

### Postman Collection

```
https://www.getpostman.com/collections/76a3ba01c9dadf68436f
```
