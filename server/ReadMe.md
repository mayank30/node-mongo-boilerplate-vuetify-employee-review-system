# Server

## Project setup

```
npm install
```

### Create .env file

```
PORT=
DB_URI=
```

### Compiles and hot-reloads for development

```
npm run start
```

### Swagger Endpoint

```
http://localhost:<port>/
```

### API Endpoint

```
http://localhost:<port>/api/<>
```

### Postman Collection

```
https://www.getpostman.com/collections/76a3ba01c9dadf68436f
```
