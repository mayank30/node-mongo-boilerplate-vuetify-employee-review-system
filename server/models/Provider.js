const mongoose = require('mongoose');

const ProviderSchema = mongoose.Schema({
    id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Provider', ProviderSchema)
