const mongoose = require('mongoose');

const ClientSchema = mongoose.Schema({
    name: { 
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true
    },
    providers: {
        type: Array,
    }
})

module.exports = mongoose.model('Client', ClientSchema)
