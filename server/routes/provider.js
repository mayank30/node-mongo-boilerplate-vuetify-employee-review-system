const router = require("express").Router();
const Provider = require("../models/Provider");
/**
 * @openapi
 * /api/provider:
 *   get:
 *     description: get all providers data
 *     responses:
 *       200:
 *         description: Returns a list of providers.
 */
router.get("/", async (req, res) => {
  try {
    const p = await Provider.find();
    res.status(200).send({ providers: p });
  } catch (error) {
    res.status(400).send(error);
  }
});

/**
 * @openapi
 * /api/provider/:id:
 *   delete:
 *     description: Delete provider by _id
 *     parameters:
 *     - name: id
 *       in: path
 *       description: _id of provider
 *     responses:
 *       200:
 *         description: Return deletedCount
 */
router.delete("/:id", async (req, res) => {
  try {
    const deleted = await Provider.remove({ _id: req.params.id });
    res.status(200).send(deleted);
  } catch (err) {
    res.status(400).send(err);
  }
});

/**
 * @openapi
 * /api/provider:
 *   post:
 *     description: Add new provider
 *     responses:
 *       200:
 *         description: return newly added provider.
 */
router.post("/", async (req, res) => {
  const c = new Provider({ ...req.body });
  try {
    const savedClient = await c.save();
    res.status(200).send(savedClient);
  } catch (err) {
    res.status(400).send(err);
  }
});
module.exports = router;
