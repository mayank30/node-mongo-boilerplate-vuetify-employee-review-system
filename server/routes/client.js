const router = require('express').Router();
const Provider = require('../models/Provider');
const Client = require('../models/Client');

/**
 * @openapi
 * /api/client:
 *   get:
 *     description: get all clients with providers data
 *     responses:
 *       200:
 *         description: Returns a list of clients and providers.
 */
router.get('/', async (req, res) => {
    try {
        const c = await Client.find({ }, { __v: false});
        const p = await Provider.find({ }, { _id: false, __v: false});
        res.status(200).send({ clients:  c, providers: p});
    } catch (error) {
        res.status(400).send(error)
    }
})

/**
 * @openapi
 * /api/client:
 *   post:
 *     description: Add new client
 *     responses:
 *       200:
 *         description: return newly added client.
 */
router.post('/',  async (req, res) => {
    const c = new Client({ ...req.body})
    try {
        const savedClient = await c.save();
        res.status(200).send(savedClient);
    } catch (err) {
        res.status(400).send(err);
    }
})

/**
 * @openapi
 * /api/client/:id:
 *   put:
 *     description: Update client information by client ID
 *     parameters:
 *     - name: id
 *       in: path
 *       description: _id of client
 *     responses:
 *       200:
 *         description: return modifiedCount
 */
router.put('/:id', async (req, res) => {
    try {
        const updatedClient = await Client.updateOne(
            { _id: req.params.id },
            {
                $set:
                {
                   ...req.body
                }
            }
        );
        res.status(200).send(updatedClient);
    } catch (err) {
        res.status(400).send(err);
    }
})

/**
 * @openapi
 * /api/client/:id:
 *   delete:
 *     description: Delete Client by _id
 *     parameters:
 *     - name: id
 *       in: path
 *       description: _id of client
 *     responses:
 *       200:
 *         description: Return deletedCount
 */
router.delete('/:id', async (req, res) => {
    try {
        const deleted = await Client.remove({ _id: req.params.id });
        res.status(200).send(deleted);
    } catch (err) {
        res.status(400).send(err);
    }
})
module.exports = router




