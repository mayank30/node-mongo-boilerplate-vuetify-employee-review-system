const app = require('express')()

require('dotenv').config()
const PORT = process.env.PORT || 3000

require('./connection')

const cors = require("cors");
app.use(cors());

// Middlewares
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())



//Routes
const clientRoute = require('./routes/client')
const providerRoute = require('./routes/provider')

//Route middilewares
app.use('/api/client', clientRoute);
app.use('/api/provider', providerRoute);
 


const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerDocs = swaggerJsDoc({
    swaggerDefinition: {
        info: {
            title: 'Protransalation REST API',
            description: "Client Provider Rest API basic crud for Protransalation Test Project"
        },
    },
    apis: ["./routes/client.js", "./routes/provider.js"]
});

app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.listen(PORT, () => {
    console.log(`Server is running on localhost:${PORT}`);
})
